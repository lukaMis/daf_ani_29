

daf_ani_29.Compass = (config) => {

  'use strict';
  const instance = {};

  let selector = 'compass';

  let needleSkipTimeout;
  let needleSkipInterval;
  let needleSkipCounter = 0;

  let chosenSide;
  let chosenText;
  let chosenAudio;

  const CLASS_SCALE_0 = 'scale-0';

  const soundControl = config.soundControl;


  const loadSvg = (data) => {
    Snap.load(data, (f) => {
      addToDom(f);
    });
  };

  const addToDom = (f) => {
    const width = f.node.viewBox.baseVal.width;
    const height = f.node.viewBox.baseVal.height;

    const compasDomString = `<div id="${selector}" class="${CLASS_SCALE_0}" data-side="no_side" data-text='' style="width: ${width}px; height: ${height}px;"></div>`;

    $('#wrapper').append(compasDomString);

    setTimeout(() => {
      Snap('#' + selector).append(f);
      $(instance).trigger('onCompasLoadComplete');
      setTimeout(() => {
        $('#' + selector).removeClass(CLASS_SCALE_0);
      }, 75);
    }, 20);
  };


  const changeSide = (side) => {
    $('#' + selector).attr({
      'data-side': side
    });
  };

  const needleSkip = () => {

    needleSkipCounter++;

    let sides = ['W', 'S', 'E', 'N'];
    let side = sides[ Math.floor(Math.random() * (sides.length)) ];

    changeSide(side);

    if (needleSkipCounter < daf_ani_29.NEEDLE_SKIP_MAX_COUNT) {
      needleSkipTimeout = setTimeout(() => {
        needleSkip();
      }, daf_ani_29.NEEDLE_SKIP_TIMEOUT);
    } else {
      needleSkipCounter = 0;

      changeSide(chosenSide);
      $('#compass #needle').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', needleHasStoped);
    }
  };


  const needleHasStoped = (e) => {
    console.log('on needle stopped');
    showLabel();
    setTimeout(() => {
      playAudio();
    }, daf_ani_29.COMPASS_AUDIO_PLAY_DELAY);
  };


  const showLabel = () => {
    $('#' + selector).attr({
      'data-text' : chosenText,
      'data-label': true
    });
  };


  const hideLabel = () => {
    $('#' + selector).attr({
      'data-text' : '',
      'data-label': false
    });
  };


  const playAudio = () => {
    $(soundControl).one('onAudioCompleted', onAudioCompletedHandler);
    soundControl.play( chosenAudio );
  };


  const onAudioCompletedHandler = (e) => {
    console.log('compass onAudioCompleted');
    $(instance).trigger('onAudioCompleted');
  };


 
  /* API */
  instance.init = (data) => {
    loadSvg(data);
  };
  instance.changeSide = (index) => {
    // changeSide(side);
    chosenSide = daf_ani_29.DATA[index].compass.side;
    chosenText = daf_ani_29.DATA[index].compass.text;
    chosenAudio = daf_ani_29.DATA[index].compass.audio;
    hideLabel();
    setTimeout( () => {
      needleSkip();
    }, 75);
  };


  console.log('Compass ready.');
  return instance;
};