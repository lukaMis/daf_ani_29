

daf_ani_29.DataHandler = () => {

  'use strict';
  const instance = {};
  

  /* API */
  instance.init = () => {
    daf_ani_29.DATA = i18n.t('data', {returnObjectTrees: true});
  };
  instance.getData = () => {
    return daf_ani_29.DATA;
  };
  instance.getCompasData = () => {
    return i18n.t('compassImage');
  };
  instance.getMarkerData = () => {
    return i18n.t('markerImage');
  };

  console.log('DataHandler ready.');
  return instance;
};