

daf_ani_29.Question = (config) => {

  'use strict';
  const instance = {};

  const SELECTOR = 'questionWrapper';
  const CLASS_OFF_CANVAS = 'off-canvas';

  const soundControl = config.soundControl;


  const addToDom = (data) => {
    const domString =
    `<div id="${SELECTOR}" class="${CLASS_OFF_CANVAS}">
      <p>${data[0].question.text}</p>
    </div>`;

    $('#wrapper').append(domString);
    setTimeout(() => {
      $('#' + SELECTOR).removeClass(CLASS_OFF_CANVAS);
    }, 75);
  };


  const playAudio = (index) => {
    $(soundControl).one('onAudioCompleted', onAudioCompletedHandler);
    soundControl.play( daf_ani_29.DATA[index].question.audio );
  };


  const onAudioCompletedHandler = (e) => {
    console.log('question onAudioCompleted');
    $(instance).trigger('onAudioCompleted');
  };


  /* API */
  instance.init = (data) => {
    addToDom(data);
  };
  instance.askQuestion = (index) => {
    playAudio(index);
  };

  console.log('Question ready.');
  return instance;
};