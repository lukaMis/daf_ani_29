

daf_ani_29.Controller = (config) => {

  'use strict';
  const instance = {};

  const compass = config.compass;
  const question = config.question;
  const marker = config.marker;  
  const dots = config.dots;

  let clickedDotId = 0;
  let autoPlay = true;
  let autoplayTimeout = null;


  const init = () => {
    addListnerToDots();
    startAutoPlay();
  };


  const addListnerToDots = () => {
    $(dots).on('onDotClicked', onDotClickedHandler);
  };


  const onDotClickedHandler = (e, data) => {
    clickedDotId = data.index;

    marker.killMarker();

    $(compass).one('onAudioCompleted', onCompasCompleted);
    compass.changeSide(clickedDotId);
  };


  const onCompasCompleted = (e) => {
    $(question).one('onAudioCompleted', onQuestionCompleted);
    setTimeout(() => {
      question.askQuestion(clickedDotId);
    }, daf_ani_29.GLOBAL_TIMEOUT_FOR_NEXT_ITEM);
  };


  const onQuestionCompleted = (e) => {
    $(marker).one('onAudioCompleted', onMarkerCompleted);
    setTimeout(() => {
      marker.makeMarker(clickedDotId);
    }, daf_ani_29.GLOBAL_TIMEOUT_FOR_NEXT_ITEM);
  };


  const onMarkerCompleted = (e) => {
    console.log('next DOT please');
    dots.enableDots();

    if( dots.checkForAutoPlay() ) {
      clickedDotId++;
      startAutoPlay();
    }
  };


  const startAutoPlay = () => {
    autoplayTimeout = setTimeout(function() {
      if(dots.checkForAutoPlay() ) {
        $($('.dot')[clickedDotId]).trigger('click', {autoPlay:true});
      } 
    }, daf_ani_29.AUTOPLAY_DELAY);
  };


  /* API */
  instance.init = () => {
    init();
  };

  console.log('Controller ready.');
  return instance;
};