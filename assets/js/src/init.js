

(() => {

  'use strict';
  $(document).one('i18nComplete', init);

  function init(e) {
    FastClick.attach(document.body);
    daf_ani_29.Bootstrap();
  };
  
})();