

daf_ani_29.Bootstrap = () => {

  'use strict';
  const instance = {};


  const dataHandler = daf_ani_29.DataHandler();


  const soundControl = daf_ani_29.SoundControl();
  $(soundControl).on('onAudioLoadComplete', (e) => {
    console.log('audio loaded');
    compass.init( dataHandler.getCompasData() );
  });


  const compass = daf_ani_29.Compass({
    soundControl: soundControl
  });
  $(compass).on('onCompasLoadComplete', (e) => {
    console.log('compass loaded');
    marker.init( dataHandler.getMarkerData() );
  });


  const marker = daf_ani_29.Marker({
    soundControl: soundControl
  });
  $(marker).on('onMarkerLoadComplete', (e) => {
    console.log('marker loaded');
    question.init( dataHandler.getData() );
    dots.init( dataHandler.getData() );
    controller.init();
    daf_ani_29.removePreloader();
  });


  const question = daf_ani_29.Question({
    soundControl: soundControl
  });


  const dots = daf_ani_29.Dots();


  const controller = daf_ani_29.Controller({
    compass: compass,
    question: question,
    marker: marker,
    dots: dots
  });

  
  dataHandler.init();
  soundControl.init( dataHandler.getData() );


  console.log('Bootstrap ready.');
  return instance;
};