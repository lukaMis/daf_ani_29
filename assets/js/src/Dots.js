

daf_ani_29.Dots = (config) => {

  'use strict';
  const instance = {};

  let isAutoPlayActive = true;


  const makeDots = (data) => {
    const dotsStart = '<div id="dotsWrapper">';
    let dotsString = '';
    const dotsEnd = '</div> ';

    for (var i = 0; i < data.length; i++) {
      dotsString += `<div id='dot-${i}' class='dot' data-active='false'></div>`;
    }
    $('#wrapper').append( dotsStart + dotsString + dotsEnd );
    setTimeout(() => {
      addListnerToDots();
    }, 25);
  };


  const addListnerToDots = () => {
    $('.dot').on('click', dotClickedHandler);
  };

  const removeListnerFromDots = () => {
    $('.dot').off('click', dotClickedHandler);
  };


  const dotClickedHandler = (e, data) => {
    removeListnerFromDots();

    if(!data) {
      console.log('user clicked');
      isAutoPlayActive = false;
    } else {
      console.log('auto click');
    }

    $('.dot').attr({
      'data-active': 'false'
    });

    $(e.target).attr({
      'data-active': 'true'
    });

    $(instance).trigger('onDotClicked', {
      index: parseInt(e.target.id.slice(-1))
    });
  };


  /* API */
  instance.init = (data) => {
    makeDots(data);
  };
  instance.enableDots = () => {
    addListnerToDots();
  };
  instance.checkForAutoPlay = () => {
    return isAutoPlayActive;
  };

  console.log('Dots ready.');
  return instance;
};