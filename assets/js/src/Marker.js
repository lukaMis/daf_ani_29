

daf_ani_29.Marker = (config) => {

  'use strict';
  const instance = {};

  let markerFragment;

  const MARKER_IMAGE = 'image';
  const MARKER_SELECTOR = 'marker';
  const MARKER_LABEL = 'label';

  const soundControl = config.soundControl;


  const loadSvg = (data) => {
    Snap.load(data, (f) => {
      markerFragment = f;
      $(instance).trigger('onMarkerLoadComplete');
    });
  };


  const makeMarker = (index) => {
    killMarker();
    
    const markerString = 
    `<div id='marker' data-hidden="true" data-label='${daf_ani_29.DATA[index].answer.marker.label}' style='top:${daf_ani_29.DATA[index].answer.marker.y}px; left:${daf_ani_29.DATA[index].answer.marker.x}px'>
      <p id='${MARKER_LABEL}' class='background-violet color-white'>${daf_ani_29.DATA[index].answer.text}</p>
      <div id='${MARKER_IMAGE}'></div>
    </div>`;

    $('#contentWrapper').append(markerString);
    setTimeout(() => {
      Snap('#' + MARKER_IMAGE).append(markerFragment);
    }, 25);
    setTimeout(() => {
      $('#' + MARKER_SELECTOR).attr({
        'data-hidden' : false 
      });
    }, 100);
    setTimeout(() => {
      playAudio(index);
    }, daf_ani_29.MARKER_AUDIO_PLAY_DELAY);
  };


  const playAudio = (index) => {
    $(soundControl).one('onAudioCompleted', onAudioCompletedHandler);
    soundControl.play( daf_ani_29.DATA[index].answer.audio );
  };


  const onAudioCompletedHandler = (e) => {
    console.log('marker onAudioCompleted');
    $(instance).trigger('onAudioCompleted');
  };


  const killMarker = () => {
    $('#contentWrapper').empty();
  };



  /* API */
  instance.init = (data) => {
    loadSvg(data);
  };
  instance.makeMarker = (id) => {
    makeMarker(id);
  };
  instance.killMarker = () => {
    killMarker();
  };

  console.log('Marker ready.');
  return instance;
};