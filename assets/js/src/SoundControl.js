

daf_ani_29.SoundControl = () => {

  'use strict';
  const instance = {};

  let filesLoaded = 0;
  let filesToLoad = 0;

  
  const loadAudio = (data) => {
    createjs.Sound.on('fileload', onSoundLoaded);
    for (let i = 0; i < data.length; i++) {
      filesToLoad++;
      createjs.Sound.registerSound( `assets/audio/${data[i].compass.audio}.mp3`, data[i].compass.audio );

      filesToLoad++;
      createjs.Sound.registerSound( `assets/audio/${data[i].question.audio}.mp3`, data[i].question.audio );

      filesToLoad++;
      createjs.Sound.registerSound( `assets/audio/${data[i].answer.audio}.mp3`, data[i].answer.audio );
    }
  };


  const onSoundLoaded = (e) => {
    filesLoaded++;
    if(filesLoaded === filesToLoad) {
      filesLoaded = null;
      filesToLoad = null;
      $(instance).trigger('onAudioLoadComplete');
    }
  };


  const playAudio = (id) => {
    const player = createjs.Sound.play(id);
    player.on('complete', onAudioCompleted);
  };


  const onAudioCompleted = (e) => {
    e.target.off('complete', onAudioCompleted);
    $(instance).trigger('onAudioCompleted');
  };


  /* API */
  instance.init = (config) => {
    loadAudio(config);
  };
  instance.play = (id) => {
    playAudio(id);
  };

  console.log('SoundControl ready.');
  return instance;
};