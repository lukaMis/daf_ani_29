

const daf_ani_29 = {};


daf_ani_29.DATA; //  fill it in DataHandler

daf_ani_29.MARKER_AUDIO_PLAY_DELAY = 2000;

daf_ani_29.COMPASS_AUDIO_PLAY_DELAY = 1000;
daf_ani_29.NEEDLE_SKIP_TIMEOUT = 200;
daf_ani_29.NEEDLE_SKIP_MAX_COUNT = 16;

daf_ani_29.GLOBAL_TIMEOUT_FOR_NEXT_ITEM = 1000;

daf_ani_29.AUTOPLAY_DELAY = 1000;

daf_ani_29.removePreloader = () => {
  $('body').removeClass('loading');
};