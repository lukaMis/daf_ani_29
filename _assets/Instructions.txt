ADOBE INDESIGN PRINTING INSTRUCTIONS FOR SERVICE PROVIDER REPORT

PUBLICATION NAME: DN 160627 DaF_ani_29 .indd

PACKAGE DATE: 24/11/16 13:08
Creation Date: 24/11/16
Modification Date: 24/11/16

CONTACT INFORMATION

Company Name: 
Contact: 
Address: 





Phone: 
Fax: 
Email: 

SPECIAL INSTRUCTIONS AND OTHER NOTES






External Plug-ins 0
Non Opaque Objects :On Page1, 2, 3, 4, A, B

FONTS
5 Fonts Used; 0 Missing, 1 Embedded, 0 Incomplete, 0 Protected

Fonts Packaged
- Name: Arial-BoldMT; Type: OpenType TrueType, Status: OK
- Name: ArialMT; Type: OpenType TrueType, Status: OK
- Name: MyriadPro-Bold; Type: Type 1, Status: Embedded
- Name: WebHostingHub-Glyphs; Type: TrueType, Status: OK
- Name: WebSymbols-Regular; Type: OpenType Type 1, Status: OK


COLORS AND INKS
4 Process Inks; 0 Spot Inks

- Name and Type: Process Cyan; Angle: 75,000; Lines/Inch: 70,000
- Name and Type: Process Magenta; Angle: 15,000; Lines/Inch: 70,000
- Name and Type: Process Yellow; Angle: 0,000; Lines/Inch: 70,000
- Name and Type: Process Black; Angle: 45,000; Lines/Inch: 70,000


LINKS AND IMAGES
(Missing & Embedded Links Only)
Links and Images: 12 Links Found; 0 Modified, 0 Missing 0 Inaccessible
Images: 0 Embedded, 0 use RGB color space


PRINT SETTINGS
PPD: Device Independent, (PostScript® File)
Printing To: Prepress File
Number of Copies: 1
Reader Spreads: No
Even/Odd Pages: Both
Pages: All
Proof: No
Tiling: None
Scale: 100%, 100%
Page Position: Upper Left
Print Layers: Visible & Printable Layers
Printer's Marks: None
Bleed: 0 px, 0 px, 0 px, 0 px
Color: Composite CMYK
Trapping Mode: None
Send Image Data: All
OPI/DCS Image Replacement: No
Page Size: 1024 x 768
Paper Dimensions: 0 px x 0 px
Orientation: Landscape
Negative: No
Flip Mode: Off


FILE PACKAGE LIST

1. DN 160627 DaF_ani_29 .indd; type: Adobe InDesign publication; size: 2936K
2. DN 160627 DaF_ani_29 .idml; type: InDesign Markup Document; size: 356K
3. DN 160627 DaF_ani_29 .pdf; type: Adobe Acrobat Document; size: 2979K
4. Arial Bold.ttf; type: Font file; size: 733K
5. Arial.ttf; type: Font file; size: 756K
6. webhostinghub-glyphs.ttf; type: Font file; size: 301K
7. WebSymbols-Regular.otf; type: Font file; size: 26K
